package br.df.mlpdleal.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class UsersXML {
	
	public static RequestSpecification reqSpec;
	public static ResponseSpecification resSpec;
	
	@BeforeClass
	public static void setUp() {
		
		RestAssured.baseURI = "https://restapi.wcaquino.me";
		//RestAssured.port = 443;
		//RestAssured.basePath = "/v2";
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.log(LogDetail.ALL);
		reqSpec = reqBuilder.build();
		
		ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
		resBuilder.expectStatusCode(200);
		resSpec = resBuilder.build();
		
		
		// specification que faz todos os testes herdarem esses comportamentos
		// ou seja, na request todos os testes vão ter log
		// e na response, todos os testes deverão ter retorno 200
		RestAssured.requestSpecification = reqSpec;
		RestAssured.responseSpecification = resSpec;
		
		
		
	}
	
	@Test
	public void devoTrabalharComXML() {
		
		given()
		.when()
			.get("/usersXML/3")
		.then()
			//.statusCode(200)
			.rootPath("user")
			.body("name", is("Ana Julia"))
			.body("@id", is("3"))
			.body("filhos.name[0]", is("Zezinho"))
			.body("filhos.name[1]", is("Luizinho"))
			.body("filhos.name", hasItem("Luizinho"))
			.body("filhos.name", hasItems("Zezinho", "Luizinho"))
		;
	}

	@Test
	public void devoFazerPesquisasAvancadas() {
		given()
			.spec(reqSpec)
		.when()
			.get("/usersXML")
		.then()
			.spec(resSpec)
			.body("users.user.size()", is(3))
			.body("users.user.findAll{it.age.toInteger() <= 25}.size()", is(2))
			.body("users.user.@id", hasItems("1", "2", "3"))
			.body("users.user.find{it.age == 25}.name", is("Maria Joaquina"))
		;
	}
	
	@Test
	public void devoFazerPesquisasAvancadasComXMLeJava() {
		String nome = given()
			.spec(reqSpec)
		.when()
			.get("/usersXML")
		.then()
			.spec(resSpec)
			.body("users.user.size()", is(3))
			.extract().path("users.user.name.findAll{it.toString().startsWith('Maria')}")
		;
		
		Assert.assertEquals("Maria Joaquina".toUpperCase(), nome.toUpperCase());
	}
	
	@Test
	public void devoFazerPesquisasAvancadasComXPATH() {
		given()
			.spec(reqSpec)
		.when()
			.get("/usersXML")
		.then()
			.spec(resSpec)
			.body(hasXPath("count(/users/user)", is("3")))
			.body(hasXPath("/users/user[@id = '1']"))
			.body(hasXPath("//user[@id = '2']"))
			.body(hasXPath("//name[text() = 'Luizinho']/../../name", is("Ana Julia")))
			.body(hasXPath("//name[text() = 'Ana Julia']/following-sibling::filhos", allOf(containsString("Zezinho"), containsString("Luizinho"))))
			
		;
	}
}
