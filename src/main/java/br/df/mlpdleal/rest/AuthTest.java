package br.df.mlpdleal.rest;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;

public class AuthTest {

	private JsonPath jsonPath;

	@Test
	public void deveAcessarSwapi() {
		given().log().all().when().get("https://swapi.dev/api/people/1").then().log().all().statusCode(HttpStatus.SC_OK)
				.body("name", Matchers.is("Luke Skywalker"));
	}

	@Test
	public void deveAcessarOpenWeatherComApiKey() {
		given().log().all().queryParam("q", "London,uk").queryParam("APPID", "<PUT HERE THE OPEN WEATHER TOKEN>")
				.queryParam("units", "metric").when().get("http://api.openweathermap.org/data/2.5/weather").then().log()
				.all().statusCode(HttpStatus.SC_OK).body("name", Matchers.is("London"));
	}

	@Test
	public void naoDeveAcessarSemSenha() {
		given().log().all().when().get("http://restapi.wcaquino.me/basicauth").then().log().all().statusCode(401);
	}

	@Test
	public void deveAcessarComSenha() {
		given().log().all().auth().basic("admin", "senha").when().get("http://restapi.wcaquino.me/basicauth").then()
				.log().all().statusCode(200).body("status", Matchers.is("logado"));
	}

	@Test
	public void deveFazerAutenticacaoComTokenJWT() {

		Map<String, String> login = new HashMap<String, String>();
		login.put("email", "rruanandrearagao@yahoo.ca");
		login.put("senha", "123456");

		String token = 
				given()
					.log().all()
					.body(login)
					.contentType(ContentType.JSON)
				.when()
					.post("http://barrigarest.wcaquino.me/signin")
				.then()
					.log().all()
					.statusCode(200)
				.extract().path("token")
				;

		given()
			.log().all()
			.contentType(ContentType.JSON)
			.header("Authorization", "JWT " + token)
		.when()
			.get("http://barrigarest.wcaquino.me/contas")
		.then()
			.log().all()
			.statusCode(200)
			.body("nome", Matchers.hasItem("teste"))
		;
		

	}

}
