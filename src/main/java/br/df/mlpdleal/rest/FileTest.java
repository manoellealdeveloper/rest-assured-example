package br.df.mlpdleal.rest;

import static io.restassured.RestAssured.given;

import java.io.File;

import org.hamcrest.Matchers;
import org.junit.Test;

public class FileTest {
	
	@Test
	public void deveObrigarEnvioArquivo() {
		given()
			.log().all()
		.when()
			.post("http://restapi.wcaquino.me/upload")
		.then()
			.log().all()
			.statusCode(404)
			.body("error", Matchers.is("Arquivo n�o enviado"))
		;
	}
	
	@Test
	public void deveEnviarArquivo() {
		given()
			.log().all()
			.multiPart("arquivo", new File("src\\main\\resource\\users.pdf"))
		.when()
			.post("http://restapi.wcaquino.me/upload")
		.then()
			.log().all()
			.statusCode(200)
			.body("name", Matchers.is("users.pdf"))
		;
	}

	@Test
	public void naoDeveEnviarArquivoGrande() {
		given()
			.log().all()
			.multiPart("arquivo", new File("src\\main\\resource\\Winium_Driver.zip"))
		.when()
			.post("http://restapi.wcaquino.me/upload")
		.then()
			.log().all()
			.time(Matchers.lessThan(2000L))
			.statusCode(413)
		;
	}
}
