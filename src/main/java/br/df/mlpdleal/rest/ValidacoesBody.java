package br.df.mlpdleal.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

public class ValidacoesBody {
	
	@Test
	public void devoValidarBodyMaisRestritiva() {
		given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200)
			.body(is("Ola Mundo!"));
		
	}
	
	@Test
	public void devoValidarBodyRestritiva() {
		given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200)
			.body(containsString("Mundo"));
		
	}
	
	@Test
	public void devoValidarBodyMenosRestritiva() {
		given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200)
			.body(is(not(nullValue())));
		
	}

}
