package br.df.mlpdleal.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import io.restassured.http.ContentType;

public class VerbosTest {
	
	@Test
	public void deveSalvarUsuario() {
		given()
			.log().all()
			.contentType("application/json")
			.body("	{\"name\" : \"Pessoa Teste\", \"age\" : 30}")
		.when()
			.post("https://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", is(notNullValue()))
			.body("name", is("Pessoa Teste"))
		;
		
	}
	
	@Test
	public void naoDeveSalvarUsuarioSemNome() {
		given()
			.log().all()
			.contentType("application/json")
			.body("	{\"age\" : 30}")
		.when()
			.post("https://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(400)
			.body("error", is("Name é um atributo obrigatório"))
		;
		
	}
	
	@Test
	public void deveSalvarUsuarioXML() {
		given()
			.log().all()
			.contentType(ContentType.XML)
			.body("<user> <name>Manoel</name> <age>30</age> </user>")
		.when()
			.post("https://restapi.wcaquino.me/usersXML")
		.then()
			.log().all()
			.statusCode(201)
			.body("user.name", is("Manoel"))
		;
		
	}
	
	@Test
	public void deveAlterarUsuario() {
		given()
			.log().all()
			.contentType("application/json")
			.body("	{\"name\" : \"Pessoa Teste\", \"age\" : 35}")
		.when()
			.put("https://restapi.wcaquino.me/users/1")
		.then()
			.log().all()
			.statusCode(200)
			.body("id", is(notNullValue()))
			.body("name", is("Pessoa Teste"))
		;
		
	}
	
	@Test
	public void devePersonalizarUrl() {
		given()
			.log().all()
			.contentType("application/json")
			.body("	{\"name\" : \"Pessoa Teste\", \"age\" : 35}")
			.pathParam("entidade", "users")
			.pathParam("userId", "1")
		.when()
			.put("https://restapi.wcaquino.me/{entidade}/{userId}")
		.then()
			.log().all()
			.statusCode(200)
			.body("id", is(notNullValue()))
			.body("name", is("Pessoa Teste"))
		;
		
	}
	
	@Test
	public void deveDeletarUsuario() {
		given()
			.log().all()
		.when()
			.delete("https://restapi.wcaquino.me/users/1")
		.then()
			.log().all()
			.statusCode(204)
		;
	}

	@Test
	public void deveSalvarUsuarioMap() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", "usuario via map");
		params.put("age", 25);
		
		given()
			.log().all()
			.contentType("application/json")
			.body(params)
		.when()
			.post("https://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", is(notNullValue()))
			.body("name", is("usuario via map"))
			.body("age", is(25))
		;
		
	}
	
	
	@Test
	public void deveSalvarUsuarioObject() {

		User user = new User("Usuario via object", 35);
		
		given()
			.log().all()
			.contentType("application/json")
			.body(user)
		.when()
			.post("https://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", is(notNullValue()))
			.body("name", is("Usuario via object"))
			.body("age", is(35))
		;
		
	}
	
	
	@Test
	public void deserializandoObjetoSalvarUsuario() {

		User user = new User("deserializando objeto", 35);
		
		User usuarioDeserializado = given()
			.log().all()
			.contentType("application/json")
			.body(user)
		.when()
			.post("https://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.extract().body().as(User.class)
		;
		
		Assert.assertEquals("deserializando objeto", usuarioDeserializado.getName());
		Assert.assertEquals(Integer.valueOf(35), usuarioDeserializado.getAge());
		
	}
	
	@Test
	public void deveSalvarUsuarioXMLSerializado() {
		
		User user = new User("Xml", 60);
		
		given()
			.log().all()
			.contentType(ContentType.XML)
			.body(user)
		.when()
			.post("https://restapi.wcaquino.me/usersXML")
		.then()
			.log().all()
			.statusCode(201)
			.body("user.name", is("Xml"))
		;
		
	}
	
	

}




